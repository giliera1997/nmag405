# NMAG 405
## Universal Algebra

Fall Semester 2020/1

### Homework 1

**Deadline**. 5 Nov 2020, 09:00

1. (10 points) A **latin square** (𝐴, ∗) is an algebra of type (2), such that for each 𝑎, 𝑏 ∈ 𝐴 there exists a unique 𝑥 ∈ 𝐴 with 𝑥 ∗ 𝑎 = 𝑏 and a unique 𝑦 ∈ 𝐴 with 𝑎 ∗ 𝑦 = 𝑏; we then denote 𝑥 by 𝑏/𝑎 and 𝑦 by 𝑎\𝑏. (For finite 𝐴 each row and each column of the multiplication table of ∗ contains every element of 𝐴 exactly once, hence the name.)
   
   A **quasigroup** is an algebra (𝐴, ∗, \\, /) of type (2, 2, 2), which satisfies the identities: 𝑦 ≈ 𝒙 ∗ (𝑥\𝑦) ≈ 𝑥\\(𝑥 ∗ 𝑦) ≈ (𝑦/𝑥) ∗ 𝑥 ≈ (𝑦 ∗ 𝑥)/𝑥.

   Let 𝐴 be a fixed set. Prove that the map Φ that assigns to every latin square (𝐴, ∗) the algebra (𝐴, ∗, \\, /) as above, and the map Ψ that forgets the operations \\, / are mutually inverse bijections between the set of latin squares and the quasigroups (with universe 𝐴).

2. (10 points) Let ℝⁿ be the 𝑛-dimensional euclidean space and 𝒞 the set of all its (topologically) closed subsets. Show that (𝒞, ∩, ∪) is a *complete lattice* and describe ⋀ and ⋁. What are the *compact elements* of this lattice? Is it an *algebraic lattice*?

3. (10 points) A map 𝑓 : 𝐿₁ → 𝐿₂ between two lattices is called **monotone** if 𝑥 ≤ 𝑦 implies 𝑓(𝑥) ≤ 𝑓(𝑦). Let 𝐿 be a complete lattice, and 𝑓 : 𝐿 → 𝐿 a monotone map. Prove that there is a **fixpoint** 𝑎 of 𝑓, i.e. a point 𝑎 ∈ 𝐿 such that 𝑓(𝑎) = 𝑎.
